import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import Configuracoes from '@/components/configuracoes'
import Controle from '@/components/controle'
import Atualizar from '@/components/atualizar'
import Criar from '@/components/criar'
import Remover from '@/components/remover'
import Visualizar from '@/components/visualizar'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/configuracoes',
      name: 'Configuracoes',
      component: Configuracoes
    },
    {
      path: '/controle',
      name: 'Controle',
      component: Controle
    },
    {
      path: '/atualizar',
      name: 'Atualizar',
      component: Atualizar
    },
    {
      path: '/criar',
      name: 'Criar',
      component: Criar
    },
    {
      path: '/remover',
      name: 'Remover',
      component: Remover
    },
    {
      path: '/visualizar',
      name: 'Visualizar',
      component: Visualizar
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
